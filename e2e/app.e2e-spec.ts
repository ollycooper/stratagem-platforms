import { StratagemPlatformsPage } from './app.po';

describe('stratagem-platforms App', function() {
  let page: StratagemPlatformsPage;

  beforeEach(() => {
    page = new StratagemPlatformsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
