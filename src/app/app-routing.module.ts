import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

import { FixtureComponent } from './fixture/fixture.component';
import { SignalsDashboardComponent } from './signals-dashboard/signals-dashboard.component';
import { MatchOddsComponent } from './match-odds/match-odds.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'fixture',
    component: FixtureComponent,
    children: [
      { path: '', component: SignalsDashboardComponent },
      { path: 'odds', component: MatchOddsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
