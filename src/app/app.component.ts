import { Component, OnInit, Inject, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private week: number;

  constructor() {
    this.week = 1;
  }

  public carouselNext() {
    $('.carousel').carousel('next');
  }

  public carouselPrev() {
    $('.carousel').carousel('prev');
  }

  public nextWeek() {
    this.week++;
  }

  public prevWeek() {
    if (this.week > 1) {
      this.week--;
    } else {
      this.week = 1;
    }
  }

  ngOnInit() {
    $('.button-collapse').sideNav();
    // TODO add these in once this is fixed https://github.com/InfomediaLtd/angular2-materialize/issues/221 (atm the defaults need to be modified in node_modules)
    $('.carousel.carousel-slider').carousel({dist: 0/*, noWrap: true, fullWidth: true*/});
  }

  ngOnDestroy() {}

  ngAfterViewInit() {}
}
