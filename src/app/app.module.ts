import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { MaterializeModule } from 'angular2-materialize';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FixtureComponent } from './fixture/fixture.component';
import { MarketMonitorComponent } from './market-monitor/market-monitor.component';
import { MarketHighlightComponent } from './market-highlight/market-highlight.component';
import { SignalsDashboardComponent } from './signals-dashboard/signals-dashboard.component';
import { MatchOddsComponent } from './match-odds/match-odds.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FixtureComponent,
    MarketMonitorComponent,
    MarketHighlightComponent,
    SignalsDashboardComponent,
    MatchOddsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterializeModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
